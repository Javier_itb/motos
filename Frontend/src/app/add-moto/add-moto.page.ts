import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-moto',
  templateUrl: './add-moto.page.html',
  styleUrls: ['./add-moto.page.scss'],
})
export class AddMotoPage implements OnInit {

  marca: String;
  modelo: String;
  year: String;
  precio: String;

  constructor(private router: Router) { }

  ngOnInit() {
  }
  guardar() {
    var foto = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];

    const form = {
      "marca" : this.marca, 
      "modelo" : this.modelo,
      "year" : this.year,
      "foto" : foto,
      "precio" : this.precio
    };
    
    const url = "http://javier-ruiz-7e3.alwaysdata.net/miapi/moto";
    fetch(url, {
      method : "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body : JSON.stringify(form)
    })
    .then(response => {
      this.router.navigateByUrl('/home');
    }); 
  }
}
