import { Component, OnInit, NgModule } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  data: Object;
  foto: String;
  marca: String;
  modelo: String;
  id: number;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;

        this.marca = this.data[0];
        this.modelo = this.data[1];
        this.foto = this.data[2];
        this.id = this.data[3];
      }
    });
  }

  ngOnInit() {
  }

  async deleteMoto(id) {
    const alert = await this.alertController.create({
      header: 'Cuidado',
      message: '¿Estas seguro de eliminar esta moto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          handler: () =>  {}
        }, {
          text: 'Vale',
          handler: () => {
            const url = "http://javier-ruiz-7e3.alwaysdata.net/miapi/delmoto/" + id;
            fetch(url, {
              "method" : "DELETE"
            })
            .then(response => {
              this.router.navigateByUrl('/home');
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
