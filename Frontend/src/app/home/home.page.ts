import { Component, Input } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  motos;

  constructor(private route: Router) {}
  ngOnInit() {}
  ionViewWillEnter() {
    this.motos = [];
    this.getMotos('http://javier-ruiz-7e3.alwaysdata.net/miapi/getmotos')
  }

  async getMotos(url){
    const respuesta = await fetch(url);
    this.motos = await respuesta.json();
  }
  detalle(marca, modelo, foto, id) {
    var navigationExtras: NavigationExtras = {
      state: {
        parametros: [marca, modelo, foto, id]
      }
    }
    this.route.navigate(['/detalle'], navigationExtras);
  }
  addMoto() {
    this.route.navigate(['/add-moto']);
  }

  
}