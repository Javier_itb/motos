const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
const { response } = require("express");
const { get } = require("http");
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd
ddbbConfig = {
   user: 'javier-ruiz-7e3_practiques',
   host: 'postgresql-javier-ruiz-7e3.alwaysdata.net',
   database: 'javier-ruiz-7e3_finalm06',
   password: '1p2q3o4w',
   port: 5432
};
//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint



// http://javier-ruiz-7e3.alwaysdata.net/miapi/getmotos
const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos ORDER BY modelo"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + '/getmotos', getMotos);

// http://javier-ruiz-7e3.alwaysdata.net/miapi/ducati
const getDucati = (request, response) => {
    var consulta = "SELECT * FROM  motos WHERE marca = 'Ducati' ORDER BY modelo"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
        console.log(results.rows);
    });
 }
app.get(baseUrl + '/ducati', getDucati);

// http://javier-ruiz-7e3.alwaysdata.net/miapi/yamaha
const getYamaha = (request, response) => {
    var consulta = "SELECT * FROM  motos WHERE marca = 'Yamaha' ORDER BY modelo"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
        console.log(results.rows);
    });
 }
app.get(baseUrl + '/yamaha', getYamaha);

// http://javier-ruiz-7e3.alwaysdata.net/miapi/honda
const getHonda = (request, response) => {
    var consulta = "SELECT * FROM  motos WHERE marca = 'Honda' ORDER BY modelo"
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
        console.log(results.rows);
    });
 }
app.get(baseUrl + '/honda', getHonda);


// http://javier-ruiz-7e3.alwaysdata.net/miapi/moto
const addMoto = (request, response) => {
   console.log(request.body);
   const { marca, modelo, year, foto, precio } = request.body
   console.log(marca, modelo, year, foto, precio);
   response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body));

   var consulta = "INSERT INTO motos(marca,modelo,year,foto,precio) VALUES('"
   +marca+"','"+modelo+"','"+year+"','"+foto+"','"+precio+"')";
   
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
   });
}
app.post(baseUrl + '/moto', addMoto);


// http://javier-ruiz-7e3.alwaysdata.net/miapi/delmoto/...
const delMoto = (request, response) => {
   const id = request.params.id
   console.log(id);
   response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body));

   var consulta = "DELETE FROM motos WHERE id = "+id+"";
   
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
   });
}
app.delete(baseUrl + '/delmoto/:id', delMoto);




//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
